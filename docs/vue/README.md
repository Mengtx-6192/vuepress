# vue 2.0

## 一. 起步
> <a href="https://cn.vuejs.org/v2/guide/">参考文档</a>

### 1. 安装
> 说明： 本文介绍的是vue-cli， html中如何使用vue.js请参考上面的文档 <br />
> 命令： npm install -g vue <br />

> I. 打开cmd或者git bash <br />
> II. 输入上面的命令  <br />
> III. 安装完成后，查看vue版本
>> <image-preview imgUrl="vue/vue-cli.png" width="200"></image-preview>

### 2. 创建新项目
> 在目标位置，打开控制台、git bash <br />
> 输入命令： vue create xxxx   // 名称自定义 <br />
>> <image-preview imgUrl="vue/create-prj.png" width="200"></image-preview>
> 安装完成后，能够看到 <br />
>> <image-preview imgUrl="vue/create-ok.png" width="200"></image-preview>

### 3. 运行项目
> cd my-vue-project 进入创建的文件夹中 <br />
> 输入运行命令, 该命令可以在package.json中查看  <br />
>> <image-preview imgUrl="vue/package.png" width="200"></image-preview>
> 运行成功后，在浏览器中打开。localhost表示只能在本地脑上打开
>> <image-preview imgUrl="vue/npm-run-serve.png" width="200"></image-preview>
> 最终打开效果如下
>> <image-preview imgUrl="vue/run-ok.png" width="200"></image-preview>


## 二. 初识结构
### 1. 项目结构
> 最简单的结构，后面会添加很多
>> <image-preview imgUrl="vue/construct.png" width="200"></image-preview>
### 2. 单文件中的结构
> 单个.vue文件中，常用的功能
>> <image-preview imgUrl="vue/shuoming.png" width="200"></image-preview>

> 1.在app.vue中需要有个id='app'。是表示vue对app下面的所有内容生效。 且template中必须有一个根，例如这里的div。 <br />

> 2.组件的使用。import负责指向到具体的目录，components负责注册，最后在template中使用。必需要这三步 <br />

> 3.注入参数，可用于组件传值。后面会说 <br />

> 4.props一般写在子组件中，比如上图，app.vue就是父，HelloWorld是子组件。在app中，hellworld上有个msg属性，表示的就是将这个值传到子组件中，子组件要是使用，就得props注册一下 <br />
>> <image-preview imgUrl="vue/son1.png" width="200"></image-preview>

> 5.watch是监听变化用的。比如我们想msg在5秒后变成'2'。可以如下图的方式去做 <br />
>> <image-preview imgUrl="vue/watch.png" width="200"></image-preview><span style="color: #bfe5b8">思考：1. setTimeout和setInterval的区别；2. 创建后如何清理；3. 为什么写在mounted里，写在created、updated、destroyed里行不行</span> <br />

> 6.computed(计算属性)：定义一个响应式的变量，msg变化时，getMsg会实时返回msg的值 <br />
>> <image-preview imgUrl="vue/computed.png" width="200"></image-preview> <span style="color: #bfe5b8">思考：computed 和 watch 有什么区别，分别适合什么样的场景</span> <br />

> 7、8、9、10 生命周期： <a href="https://cn.vuejs.org/v2/api/#%E9%80%89%E9%A1%B9-%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F%E9%92%A9%E5%AD%90" target="_blank">点击查看详细说明</a> <br />
>> <span style="color: #bfe5b8">思考：弄清楚生命周期的详细使用，并且知道何时在哪个周期中操作</span>

> 11.方法
>> <image-preview imgUrl="vue/methods.png" width="200"></image-preview> <span style="color: #bfe5b8">思考：1. 除了点击事件外还有其他什么事件，如何写，例如: 鼠标移入移出等。2. 方法中如何传递参数</span> <br />


## 三. 指令

### 1. v-if、 v-else-if、 v-else
```vue
<template>
    <div class="hello">
        <!-- 如果msg有值 -->
        <div v-if="msg">{{ msg }}</div>
        <!-- 如果msg的值等于1 -->
        <div v-else-if="msg === 1">{{ msg }}</div>
        <!-- 否则 -->
        <div v-else>{{ msg }}</div>
    </div>
</template>
```
### 2. v-show
```vue
<template>
    <div class="hello">
        <!-- 如果msg有值，区别于if：没有 v-else-show -->
        <div v-show="msg">{{ msg }}</div>
    </div>
</template>
```
:::tip
思考： v-if 和 v-show 的区别
:::
### 3. v-for
>> 在html中，如果要展示以下数据，有几个就要写几个。
>> <image-preview imgUrl="vue/vfor.png" width="200"></image-preview>
>> 在vue中，可以使用循环指令，用数据驱动视图来展示需要达到的效果
>> <image-preview imgUrl="vue/vfor2.png" width="200"></image-preview>
```vue
// 使用方法如下：
<template>
    <div class="hello">
        <div>
            <div v-for="(item, k) in divs1" :key="k">{{ item }}</div>
            <!-- 注意div2的取值方法 -->
            <!-- <div v-for="(item, k) in divs2" :key="k">{{ item.a }}</div> -->
        </div>
    </div>
</template>

<script>
export default {
    name: "HelloWorld",
    data() {
        return {
            divs1: [1, 2, 3, 4, 5],
            // divs2: [
            //     { a: 1 },
            //     { a: 2 },
            //     { a: 3 },
            //     { a: 4 },
            //     { a: 5 },
            // ],
        };
    },
};
</script>
```
### 4. v-model
```vue
<template>
    <div class="hello">
        <!-- vue是双向数据流。msg初始为1，input的值显示为1，修改input的值为2，msg也会变成2。 -->
        <input v-model="msg" type="text" @change="change" />
    </div>
</template>

<script>
export default {
    name: "HelloWorld",
    data() {
        return {
            msg: 1,
        };
    },
    methods: {
        change() {
            // 当输入框的值发生变化时，打印出值
            console.log("this.msg", this.msg);
        },
    },
};
</script>
```
:::tip
注意：v-model只能使用在有value($refs.xxx下能够看到value)属性的组件上
:::

### 5. v-once
```vue
<template>
    <div class="hello">
        <!-- 只执行一次，msg在mounted中被改变后，页面也不会重新渲染 -->
        <p v-once>{{ msg }}</p>
    </div>
</template>

<script>
export default {
    name: "HelloWorld",
    data() {
        return {
            msg: 1,
        };
    },
    mounted() {
        setTimeout(() => {
            this.msg = 2;
        }, 1000);
    },
};
</script>
```
### 6. v-html、 v-text
```vue
<template>
    <div class="hello">
        <div v-html="html"></div>

        <!-- 和v-html的区别就是，不会编译标签，一切都视作字符串 -->
        <!-- <div v-text="html"></div> -->
    </div>
</template>

<script>
export default {
    name: "HelloWorld",
    data() {
        return {
            html: '<div style="color:red">使用v-html渲染</div>',
        };
    },
};
</script>
```
> v-html：
>> <image-preview imgUrl="vue/vhtml.png" width="200"></image-preview>
> v-text：
>> <image-preview imgUrl="vue/vtext.png" width="200"></image-preview>

## 四. 路由
> 安装： npm install vue-router
>> 默认是4.x的版本，使用vue3.0。这里是vue2，所以 npm install vue-router@3
>> <image-preview imgUrl="vue/vrouter.png" width="200"></image-preview>

> 说明：什么是路由？
>> 路由就是从A页面跳转到B页面，A页面的地址就是路由A，B页面的地址就是路由B
### 1. 配置路由路径
> 新建一个文件夹，存放配置文件，如果存在了，可以直接再里面修改
>> <image-preview imgUrl="vue/vrouter2.png" width="200"></image-preview>
> 重点，必须要写上router-view
>> <image-preview imgUrl="vue/vrouter3.png" width="200"></image-preview>

```js
import Vue from "vue";
import VueRouter from "vue-router";
import PageA from "../pages/pageA"; // 引入页面A
import PageB from "../pages/pageB"; // 引入页面B

Vue.use(VueRouter);
export default new VueRouter({
    routes: [
        {
            path: "/",
            redirect: "/pageA", // 路径默认跳转到 pageA页面
        },
        {
            path: "/pageA", // 即，浏览器中输入 localhost:8080，就会跳转到 localhost:8080/pageA
            component: PageA,
        },
        // 这种表示含有动态参数id，使用参照 路由跳转中的代码
        // {
        //     path: "/pageA/:id",
        //     component: PageA,
        // },
        {
            path: "/pageB",
            component: PageB,
        },
        // 同理，可以在下面继续配置其他页面
        {
            ...
        }
    ],
});

```

### 2. 配置跳转页面
> <image-preview imgUrl="vue/pages1.png" width="200"></image-preview>

> 在浏览器中输入： http://localhost:8081/ 会跳转到 http://localhost:8081/pagesA
> 输入http://localhost:8081/pagesB，显示pagesB的内容
>> <image-preview imgUrl="vue/pages2.png" width="200"></image-preview>


### 3. 路由跳转

#### a). 方法一：

> 点击router-link 完成跳转的效果
> <image-preview imgUrl="vue/routerlink1.png" width="200"></image-preview>
:::tip
    从上图可以看出，结构可以分成上下两部分：

    1. 上面由两个router-link构成一个flex布局

    2. 下面则展示页面的内容，从这里可以理解
        router-view类似于占位符，把页面的位置先占着。
        等到需要展示的时候把内容显示出来
:::

#### b). 方法二:
> 点击span 完成跳转的效果。
> <image-preview imgUrl="vue/routerlink2.png" width="200"></image-preview>
:::tip
 可百度搜索this.$router.push()有哪几种使用方法

 以及，this.$router.replace() 等等相关方法
:::

### 4.路由传参

> 路由传参可满足页面之间跳转，且需要某个其他页面才有的参数

```vue
<template>
    <!--
        这里的 12345678 对应上面 配置路由路径 /pageA/:id 的 id。
        因为在配置的时候，这个值不确定是什么，所以相当于申明个变量id，
        后面跳转的时候加的值，就是把值12345678赋值给id的意思
    -->
    <router-link to="/pageA/12345678"></router-link>
</template>
<script>
    export default {
        methods: {
            clickFn() {
                let id = 12345678
                this.$router.push(`/pageA/${id}`)
            }
        }
    }
</script>
```

```vue
<template>
    <div>
        <div>欢迎来到 page - A 页面</div>
        <hr />
        <p>{{ params.id }} -- {{ query.timer }}</p>
    </div>
</template>

<script>
export default {
    data() {
        return {
            params: this.$route.params,
            query: this.$route.query,
        };
    },
    mounted() {
        // 注意：这里是 $route，而不是 $router  !!!
        console.log(this.$route);
    },
};
</script>
```
> <image-preview imgUrl="vue/vrouter4.png" width="200"></image-preview>
>> 可以看到url上 12345678 在打印出来的值上对应的是 params.id (id是 /pages/:id 中的id)，问号后面的timer对应的是 query

:::tip
    思考：
        1. this.$route 和 this.$router 有什么区别
        2. query 和 params 有什么区别
:::

## 五. 属性的使用
### 1. 动态class
> 从上图可以看到 ':class'
>> vue中 :xxx=""   表示将某个值传递到xxx属性上去，同理： <br />
>> vue 也有class属性，即将后面的值 传递给 class

```vue

<!--
    注意：带引号的'active'表示只是字符串，是类名
        不带引号的 active 是变量，是可以动态变化的
-->
<template>
    <div id="app">
        <!-- 方式一 -->
        <span :class="'active'">pageA</span>
        <!--
        //等价于
        <span class="active">pageA</span>
        -->

        <!-- 方式二 -->
        <span :class="['tac', 'active']">pageA</span>
        <!--
        // 等价于
        <span class="tac active">pageA</span>
        -->

        <!-- 方式三 -->
        <span :class="[active ? 'active' : '']">pageA</span>
        <!--
        // active为true时等价于
        <span class="active">pageA</span>
        // active为false时等价于
        <span class="">pageA</span>
        -->

        <!-- 方式四 -->
        <span :class="[{ 'active': active }, 'tac']">pageA</span>
        <!--
        // active为true时等价于
        <span class="active, tac">pageA</span>
        // active为false时等价于
        <span class=" tac">pageA</span>
        -->

        <!-- 方式五 -->
        <span :class="{ 'active': active, 'tac': true }">pageA</span>
        <!-- 等价于 -->
        <span class="tac">pageA</span>

    </div>
</template>

<script>
export default {
    name: "App",
    data() {
        return {
            active: true,
        };
    },
};
</script>
```

### 2. 动态style

> 同理，style也可以根据条件来设置

```vue
<template>
    <div id="app">
        <!-- 方式一 -->
        <span :style="{ color: 'red', fontSize: size + 'px'}">pageA</span>
        <!--
        //等价于
        <span style="color: red; fontSize: 16px">pageA</span>
        -->

        <!-- 方式二 -->
        <span :style="{ color: active ? 'red' : 'yellow', fontSize: size2 }">pageA</span>
        <!--
        // 等价于
        <span style="color: yellow; fontSize: 18px">pageA</span>
        -->

    </div>
</template>

<script>
export default {
    name: "App",
    data() {
        return {
            active: false,
            size: 16,
            size2: '18px'
        };
    },
};
</script>
```


:::tip
    扩展：使用变量代替在标签上的class、style
:::

```vue
<template>
    <div id="app">
        <span :class="getClass">pageA</span>
        <!--
        //等价于
        <span class="active tac">pageA</span>
        -->

        <span :style="getStyle">pageA</span>
        <!--
        //等价于
        <span style="color: 'red'; fontSize: '16px'">pageA</span>
        -->
    </div>
</template>

<script>
export default {
    name: "App",
    data() {
        return {
            getClass: ['active', 'tac'],
            getStyle: {
                color: 'red',
                fontSize: '16px'
            }
        };
    },
};
</script>
```

### 3. 计算属性(computed)
> 说明：指将某个变量(size)，按照新需求重新组装成一个新的变量(getSize)，且在size值发生变化时(例如：定时器1秒后，16变成18)，getSize会自动再执行一遍，以确保getSize获取到的是最新的size(18)

> 承接上文，如果颜色、文字大小的值来自于父组件，或其他地方传过来的

```vue

<!-- 父组件 -->
<template>
    <div id="app">
        <Son :color="'red'" :size="size"></Son>
    </div>
</template>
<script>
export default {
    data() {
        return {
            size: 16
        }
    },
    mounted() {
        setTimeout(() => {
            this.size = 18
        }, 1000)
    }
}
</script>
```
```vue
<!-- 子组件 -->
<template>
    <div id="app">
        <span :style="getStyle">pageA</span>
    </div>
</template>

<script>
export default {
    props: {
        color: String,
        size: Number
    },
    computed: {
        getSize() {
            return this.size + 'px'
        },
        getStyle() {
            return {
                color: this.color,
                fontSize: this.size + 'px'
            }
        }
    },
};
</script>
```


### 3. 监听(watch)

> 某个值发生变化时，会触发

```vue

<template>
    <div id="app">
        <span>{{ newSize }}</span>
    </div>
</template>

<script>
export default {
    data() {
        return {
            newSize: 0
        }
    },
    props: {
        color: String,
        size: Number
    },
    watch: {
        size(newVal, oldVal) {
            // 只要size的值发生了变化，就会进入到这个里面
            // newVal 新值，比如 size变化之后的值，也就是18
            // oldVal 旧值，size变化之前的值，为16

            this.newSize = newVal
        }
    },
};
</script>
```


:::tip
    思考： watch和computed有什么不同，两者分别在什么时候使用
:::


## 六. vuex

> <a href="https://vuex.vuejs.org/">查看介绍说明</a>

> 安装： npm i vuex (默认4.x版本，这里使用 npm i vuex@3.0.0)
>> <image-preview imgUrl="vue/vuex1.png" width="200"></image-preview>

> 使用
>> <image-preview imgUrl="vue/vuex2.png" width="200"></image-preview>

### 1. state
> 方法一：this.$store.state.count
>> <image-preview imgUrl="vue/vuex3.png" width="200"></image-preview>
> 方法二：
>> <image-preview imgUrl="vue/vuex3-2.png" width="200"></image-preview>

::: tip
    从上面的例子可以看出，在store文件夹下的index.js中写入的值，在其他文件中都可以用到
    那么，是否可以将pageA的值存入，在pageB页面取出来用？ 请参考mutation
:::


### 2. getter
> 和 vue 中的 computed 相似，是对 state 中的值重新拼接。并且当 state 中的值被修改后，getter会重新执行一次。保证当前取到的值是最新的
>> <image-preview imgUrl="vue/vuex-getter1.png" width="200"></image-preview>
```js
    // 当然也可以直接取值
    console.log(this.$store.getters.getCount);
```



### 3. mutation
>> <image-preview imgUrl="vue/vuex4.png" width="200"></image-preview>
```vue
<template>
    <div>
        <div>欢迎来到 page - A 页面</div>
    </div>
</template>

<script>
import { mapMutations } from "vuex";
export default {
    mounted() {
        this.changeCount(1234343);
    },
    methods: {
        ...mapMutations(["changeCount"]),
    },
};
</script>
```
:::tip
    pageB从store里使用count的值，此时count = 0。
    同样的，pageA也操作修改了count的值，
    结果就是pageA修改了值后，pageB也能取到最新的值。
    所以 父子组件传值、页面传值。除了之前学的那些方法，也可以使用vuex来完成
:::

### 4. action

> 同样的，也是可以使用方法来改变值。但和mutation的区别是，action一般用于处理异步函数，通俗理解为调用接口
>> <a href='#qi'>接口调用参考 '七. axios'</a>
>> <image-preview imgUrl="vue/vuex-actions.png" width="200"></image-preview>



### 5. module
> 模块化，即没个页面都可对应一个自己的store。
>> <a href="https://zhuanlan.zhihu.com/p/98083457">参考地址，或自行百度vuex module</a>


<div id='qi'></div>

## 七. axios
> 前端ajax的工具库，详情参考html章节。或百度查找<a href="https://blog.csdn.net/weixin_42529972/article/details/109469628">JS-ajax详解</a>

> <a href="http://www.axios-js.com/zh-cn/docs/index.html">点击查看axios详细文档说明</a>

### 1. 安装
> 命令：npm install axios
>> <image-preview imgUrl="vue/axios1.png" width="200"></image-preview>

> 使用：
>> <image-preview imgUrl="vue/axios2.png" width="200"></image-preview>
>> <image-preview imgUrl="vue/axios3.png" width="200"></image-preview>
>>> <a href="#">then/catch 参考 html -> '三. 常用js语法' -> f.promise对象</a>

### 2. 调用结果
> <image-preview imgUrl="vue/axios4.png" width="200"></image-preview>
```vue
<template>
    <div>
        <div>欢迎来到 page - A 页面</div>
    </div>
</template>

<script>
import { getTestList } from "../api";
export default {
    mounted() {
        getTestList()
            .then((v) => {
                console.log("getTestList", v);
            })
            .catch((err) => {
                console.log("err", err.message); // err Request failed with status code 404
            });
    },
};
</script>
```
:::tip
    这里需要了解：
        1. 代理：即，别人的接口地址可能是 http://192.168.11.12/api/test。
            这里是localhost:8081/api/test，所以是404找不到
        2. 状态：百度 'http 状态码'
:::


### 3. 代理
> 新建vue.config.js
>> <image-preview imgUrl="vue/axios5.png" width="200"></image-preview>
> 修改代理后，重新调用
>> <image-preview imgUrl="vue/axios6.png" width="200"></image-preview>
```vue
<template>
    <div>
        <div>欢迎来到 page - A 页面</div>
        <hr />
        <div>{{ result.message }}</div>
        <div v-for="(item, k) in result.data">{{item.code}}: {{item.name}}</div>
    </div>
</template>

<script>
import { getTestList } from "../api";
export default {
    data() {
        return {
            result: {}
        }
    },
    mounted() {
        getTestList()
            .then((v) => {
                console.log("getTestList", v);
                //
                console.log(v.data.message) // 成功
                console.log(v.data.code) // 0
                console.log(v.data.data) // 一个长度为4947的数组
                this.result = v.data
            })
            .catch((err) => {
                console.log("err", err.message);
            });
    },
};
</script>
```
> 最终展示效果如下：
>> <image-preview imgUrl="vue/axios7.png" width="200"></image-preview>


```js
// 常用的接口方法，需要掌握：1. 有什么区别；2. 如何传入参数
axios.get("xxxxxx");
axios.post("xxxxxx");
axios.delete("xxxxxx");
axios.put("xxxxxx");
```

