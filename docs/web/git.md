# git

## 一. 安装
<a target="_blank" href="https://git-scm.com/downloads">点击下载页面</a>

> 成功后，可在windows开始菜单中看到
<image-preview imgUrl="web/git/install-success.png" width="200"></image-preview>

<div id="er"></div>

## 二. 从零创建一个仓库
> 1.需要github、gitlab、gitee中至少有一个账号

> 2.这里以<a target="_blank" href="https://gitee.com/">gitee(点击打开官网)</a>为例
>> 登录后，新建一个仓库
>> <image-preview imgUrl="web/git/create-register1.png" width="200"></image-preview>

> 3.自定义一个仓库名称
>> <image-preview imgUrl="web/git/create-register2.png" width="200"></image-preview>

> 4.点击完成后，可以看到一个空的仓库
>> <image-preview imgUrl="web/git/create-register2-2.png" width="200"></image-preview>
>> <span style="color: red">目前都是使用https的，ssh是高级用法，以后再说</span>

> 5.在电脑中某个位置，新建一个文件夹。初始化本地仓库， <a href="#san-2">git bash打开方式可参考 三- 2</a>
>> <image-preview imgUrl="web/git/init.png" width="200"></image-preview>

> <span id="er-6">6.新建文件后，将本地和线上仓库关联起来。并将本地代码推送到远程线上</span>
>> <image-preview imgUrl="web/git/push1.png" width="200"></image-preview>
>> a).git add ./   <br />说明：将所有修改、新建的文件暂存，等待push到远程仓库中。如果git add test2.txt 表示只暂存这一个文件 <br /> <br />
>> b).git commit -m 'xxx'  <br />说明：将暂存起来的文件，统一备注 'xxxx'，引号中随便写。 下一步push到远程仓库中 <br /> <br />
>> c).git add remote origin 'https://xxxx'  <br />说明：本地关联远程仓库，一般一个本地仓库只需要关联一次即可， origin可以自定义 <br /> <br />
>> <span id="er-6-d"></span> <br />
>> d).git push origin master  <br />说明：推送代码到远程仓库， <span style="color: #c97c39ad">origin必须和上面定义的一致，master表示某个分支名</span> <br /> <br />

> 7.push成功后，刷新浏览器页面，可以看到上传的内容
>> <image-preview imgUrl="web/git/push1.png" width="200"></image-preview>
>> <image-preview imgUrl="web/git/create-register2-3.png" width="200"></image-preview>


## 三. clone别人的仓库
> 1.首先会有一个地址，比如上面刚刚创建的仓库(remote add origin 后面的地址), 在浏览器打开后，找到下面的按钮
>> <image-preview imgUrl="web/git/clone1.png" width="200"></image-preview>
>> <image-preview imgUrl="web/git/clone2.png" width="200"></image-preview>

<div id="san-2"></div>

> 2.在新建的文件夹中
>> git bash 就是最开始安装的git。cmd是windows自带的。都可以使用
>> <image-preview imgUrl="web/git/git-cmd.png" width="200"></image-preview>

> 3.克隆代码到本地(仅第一次使用)
>> <image-preview imgUrl="web/git/clone.png" width="200"></image-preview>
>> 如果远程仓库中有代码变更，可以使用git pull origin master拉取最新代码（<a href="#er-6-d">origin和master参考 二-6-d 中的说明</a>）
>> <image-preview imgUrl="web/git/pull1.png" width="200"></image-preview>
>> 如果本地自己修改了代码，就要重复 <a href="#er-6">git add ./ -> git commit -m 'xx' -> git push 的过程</a>

## 四. 创建新分支
> 1.分支的更新和查看
>> <image-preview imgUrl="web/git/分支1.png" width="200"></image-preview>

> 2.本地创建一个新分支
>> <image-preview imgUrl="web/git/checkout.png" width="200"></image-preview>
>> git checkout -b xxx master  (xxx可以随意命名。master也可以是其他分支，但必须是已存在的分支) <br />
>> 这时候branch -a 查看，发现本地有feature/mtx，但是远程还没有该分支。因为创建完分支后，需要push下代码
>> <image-preview imgUrl="web/git/push2.png" width="200"></image-preview>
>> 成功后，再次git fetch获取最新分支，git branch -a 查看所有分支，发现本地和远程中都含有feature/mtx分支

<div id="wu"></div>

## 五. 解决冲突
> 当两个人同时开发同一个文件时，修改了同一个地方。这时候后上传代码的人需要处理这里，是使用自己的还是别人的
>> <image-preview imgUrl="web/git/练习3-1.png" width="200"></image-preview>
>> 假如test1文件夹先提交了代码, 如下图一切正常
>> <image-preview imgUrl="web/git/练习3-2.png" width="200"></image-preview>
>> 这时候test1-2再上传就会发现报错了。大致就是说push(上传)前，需要先pull(拉取)一下。因为别人已经上传了最新的代码
>> <image-preview imgUrl="web/git/练习3-3.png" width="200"></image-preview>
>> 使用git pull 命令拉取代码，可以看到明显的冲突
>> <image-preview imgUrl="web/git/练习3-4.png" width="200"></image-preview>
>> 使用vscode打开 点击划线地方的按钮。或直接打开文件，将冲突的地方删掉
>> <image-preview imgUrl="web/git/练习3-5.png" width="200"></image-preview>
> 解决完了后，再次add\commit\push就可以了

<br />
<hr style="height:1px;border:none;border-top:1px dashed #0066CC;" />
<br />

## 五. 基础命令
#### 1.新建仓库
> git init  <br />
> git remote add origin 'http://xxxxxx' <br />
> npm config set registry http://192.168.1.181:4873 // 设置npm安装ip

##### 2. 获取远端分支
> 如果是新的仓库，这里只有master分支 <br />
>> 使用命令： git fetch 获取远端所有分支
>> 查看本地分支 git branch <br />
>> 查看远端分支 git branch -r <br />
>> 查看所有分支 git branch -a

> 如果远程分支被删，本地还有被删除的分支
>> 删除本地分支： git branch -d xxx


##### 3. 切换分支
> 切换分支： git checkout xxxx <br />
> 新建分支： git branch xxxx <br />
> 新建并且切换到对应分支： git checkout -b 'xxx' origin/xxxx <br />
>> xxx表示本地的名字，origin/xxxx 表示远端的分支名字

##### 4. 放弃修改
> 已经add的代码 <br />
>> git reset HEAD xxx / git resrt HEAD . 撤销指定文件 / 所有文件

> 未add的代码
>> git checkout ./  放弃所有已经修改的文件！！！注意慎用

##### 5. 回退版本
> git log 获取提交历史 <br />
> git reset --hard HEAD^ 回退上一次 <br />
> git reset --hard commitid 'xx' 回退指定历史版本 <br />
> git reflog 也是获取提交历史 <br />
> git reset HARD

#### 6. 合并
> 1.将代码提交到当前分支 <br />
> 2.切换到要合并的分支，比如将a合并到master <br />
>> git checkout master <br />
>> git merge a <br />
>> git push origin master <br />
注： 切换到新分支最好拉取代码解决冲突后再提交

## 六. 配置用户名、密码、邮箱（全局加上 --global）
> git config user xxxx <br />
> git config email xxxx <br />
> git config password xxxx
:::tip 提示
但是每次拉取、推送，还需要输入用户名、密码，所以配置： <br />
`git config --global(全局时使用) credential.helper store` <br />
:::


## 七. 代码关联多个仓库
> 本地项目 project，现在要同时上传到 githubA、githubB
> 1. project已经是拉取的项目A
> 2. git remote set-url --add origin http://githubB
> 3. git push origin master
```text
如果报错：
    有一个项目提示要先拉取再提交，那么就单独关联一下，即
    1.
        git remote add other htt://githubB
        git pull other master （将代码拉到本地，冲突合并等等处理完后）
        git pull origin master (将最新的代码也拉取到本地)
        git push origin master (最后直接推送即可)
        最后可以 git remote remove other
    2.
        git push origin master:master (当前本地所在分支:远程分支)
```


## 八. 练习

> 1.申请<a target="_blank" href="https://gitee.com/">gitee（建议使用）</a>、github、gitlab中任意一个网站， <a href="#er">并按照 二 创建一个仓库 remote1</a>

> 2.对 remote1 新建一个分支 xxx，并且给该分支推送一个新文件 a 。练习完成后将本地、远程的分支都删掉

> 3.冲突练习，<a href="#wu">参考五</a>
>> 重新创建一个新文件夹b，拉取 remote1 代码。
>> 切换到 xxx 分支
>> 文件夹a、b, 都修改文件a的内容(内容不能一样， 比如在a中改成 '这是文件夹a修改的内容'，b同)
>> 先提交文件夹a的代码，随后在b中提交。此时会出现冲突，应该提交不上去