# nrm

## 一. 什么是nrm

### 1. 说明
> nrm 是一个 npm 源管理器，允许你快速地在 npm源间切换。例如使用淘宝镜像，公司内部使用公司独立的镜像

### 2. 下载安装
> npm i -g nrm
>> 前提条件必须已经安装了node或者nvm，<a href="http://localhost:8080/web/nvm.html">点击跳转node安装方法</a>

### 3. 查看版本
> 完成后在命令行中输入nrm -v查看版本
>> <image-preview imgUrl="web/nrm/nrm-v.png" width="200"></image-preview>

### 4. 常用命令
```txt
nrm ls
nrm add 源名称   添加源 例: nrm add rongzer http://192.168.1.181:4873/
nrm use 源名称   使用已添加的源 例：nrm use rongzer
nrm del 源名称   删除已添加的源 例：nrm del rongzer
nrm test 源名称  测试源的的速度 例：nrm test rongzer
nrm --version 查看nrm版本
```
>> <image-preview imgUrl="web/nrm/nrm-ls.png" width="200"></image-preview>