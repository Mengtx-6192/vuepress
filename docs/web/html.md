# html

## 一. 基础知识

### 1. 从0了解html5
> HTML的全称为超文本标记语言，是一种标记语言。 这里介绍为模板的使用方法

> a).创建一个html文件 <br />
>> 下载 <a href="http://vscode.bianjiqi.net/" target="_blank">vscode</a>、subline、webstorm等其中某一个编辑器 <br />
>> 安装完成后
>>><image-preview imgUrl="web/html/vscode1.png" width="200"></image-preview>
>>> <image-preview imgUrl="web/html/vscode2.png" width="200"></image-preview>
>>> <image-preview imgUrl="web/html/vscode3.png" width="200"></image-preview>
>> 这样，一个简单的模板就生成完了。常用的几个标签如下图
>>> <image-preview imgUrl="web/html/vscode4.png" width="200"></image-preview>
>>> style是样式标签，比如说给div中的111颜色设置为红色, <a href="#yi-2">css相关参考 一.2 条</a> <br />
>>> div是块级标签，详细说明参考第a)条； <br />
>>> script是写js逻辑的地方，比如说点击按钮弹个提示窗口。 <br />
>> 浏览器中效果如下：
>>> <image-preview imgUrl="web/html/vscode5.png" width="200"></image-preview>

> <span style="color: red">作业： 新建一个页面，在页面中展示 script中src是什么含义，以及除了这个src还有什么其他的功能</span>


> b).了解 块级元素、行内元素
>> <image-preview imgUrl="web/html/inline-block.png" width="200"></image-preview>
>> 可以看到 div中111的内容独占一行。而span 中的2222和3333相连的。
>>> 块级元素(<span style="color: red">独占一行，可修改尺寸大小</span>)： div、p、h1~h6、ul、ol、dl、li、table、hr等 <br />
>>> 行内元素(<span style="color: red">不独占，且不可修改大小</span>)： a、span、i等 <br />
>>> 行内块元素(<span style="color: red">不独占，但可修改大小</span>)： input、img等 <br />

> c).了解标签上的属性，比如a标签。可以跳转到指定连接，是打开一个新页面，还是在当前页面打开
>> <a href="https://www.runoob.com/tags/tag-a.html" target="_blank">打开新页面</a> <br />
>> <a href="https://www.runoob.com/tags/tag-a.html">在当前页面打开</a> <br />

<div id='yi-2'></div>

### 2. css和css3
> a).必须要先有一定的了解，可以参考以下文档<br />
>> <a href="https://www.runoob.com/css/css-tutorial.html" target="_blank">点击了解css</a> <br />
>> <a href="https://www.runoob.com/css3/css3-tutorial.html"  target="_blank">点击了解css3</a>

> b). 常用样式简介
>> I. 选择器
>>> ```html
>>> <head>
>>>     <title>Document</title>
>>>     <style>
>>>         /* 标签选择器，直接使用标签名 */
>>>         div {}
>>>         /* id选择器，一个标签只能有一个id，但可以有多个名字，id='div1 div2' */
>>>         #div1 {}
>>>         /* class选择器， 同上。一般用于多个div，每个div的样式都不一样的时候 */
>>>         .box1 {}
>>>         /* 属性选择器：标签|id|class选择器 + 属性的值 */
>>>         input[type='button'] {}
>>>         /* 所有的class为box1，box2的 */
>>>         .box1,.box2 {}
>>>         /* 表示 box2 里面(不管有多少层)，class为span2的那个标签 */
>>>         .box2 .span2-1 {}
>>>         /* 表示 box2 里面(只能是下一层)，class为span2的那个标签 */
>>>         .box2 > .span2-2 {}
>>>     </style>
>>> </head>
>>> <body>
>>>     <div id="div1" class="box1">111111</div>
>>>     <div id="div2" class="box2">
>>>         <div>
>>>             <span class="span2-1"></span>
>>>         </div>
>>>         <span class="span2-2"></span>
>>>     </div>
>>>     <input type='button' />
>>>     <input type='checkbox' />
>>> </body>
>>> ```
>> II. 颜色等
>>>```css
>>> div {
>>>    /* 字体的大小， font有很多属性，比如下面的size。其他可参考文档说明 */
>>>    font-size: 12px
>>>    /* 字体的颜色 */
>>>    color: red
>>>    /* 背景的颜色，background有很多属性，color表示颜色。其他可参考文档说明 */
>>>    background-color: yellow
>>>    /* 边框：1px的粗细，有虚线、实线等，颜色是绿色。具体参数可参考文档说明 */
>>>    border: 1px solid green
>>>    ...
>>> }
>>>```
>> III. 边距、定位、伪元素等
>>>```css
>>> div {
>>>    /* 外边距：div盒子外面的，例如手机带个壳，宽度发生了变化，就是外边距 */
>>>    四种方式：1. margin：10px；
>>>             2. margin 10px 20px;
>>>             3. margin: 10px 20px 30px;
>>>             4. margin: 10px 20px 30px 40px
>>>    /* 内边距：div盒子里面的，例如手机屏幕四周的黑边 */
>>>    padding: 10px // 用法同上也是4种
>>>    /* 定位：相对定位|绝对定位|固定定位|默认定位等。具体参数可参考文档说明 */
>>>    position: relative|absloute|fiexd|static 等
>>> }
>>> /* 伪元素，after、before，每个标签上都有一组 */
>>> div::after {}
>>> div::before {}
>>>```
>> IV. 盒模型
>>> 上面说到margin、padding的区别，如果div宽高有100px。 <br />
>>>     margin：10px, 这时候应该的效果是div宽高变成了110px, 但是内容占据的依然是100px <br />
>>>     padding：10px, 这时候应该的效果是div宽高还是100px, 但是内容占据变成了90px <br />
>>> ```css
>>>  /* 该属性可以控制，是div显示100，还是内容显示100 */
>>>  box-sizing: border-box|content-box
>>> ```
>>> V. 布局方式： flex、grid等
>>>```html
>>>  <!-- 将以下代码复制到html文件中，保存后在浏览器打开查看效果 -->
>>>  <!-- flex -->
>>>  <div>flex布局：</div>
>>>  <div style="display: flex;">
>>>      <span style="flex: 1">1</span>
>>>      <span style="flex: 2">2</span>
>>>  </div>
>>>
>>>  <p>-------分割线-------</p>
>>>  <hr>
>>>  <p>-------分割线-------</p>
>>>  <!-- grid -->
>>>  <div>grid布局：</div>
>>>  <div style="display: grid; grid-template-columns: 100px 100px 100px; grid-template-rows: 100px 100px 100px;">
>>>      <span>1</span>
>>>      <span>2</span>
>>>      <span>3</span>
>>>      <span>4</span>
>>>  </div>
>>>```
>>> <image-preview imgUrl="web/html/flex-grid.png" width="200"></image-preview>

### 3. js
> a).必须要先有一定的了解，可以参考以下文档<br />
> <a href="https://www.runoob.com/js/js-tutorial.html" target="_blank">点击了解js</a> <br />

> b).js六大基本数据类型
>> String字符串 ： ''、 'xxxxx'、'123'<br />
>> Number数字型：123<br />
>> Boolean布尔型：true, false<br />
>> Null型：null<br />
>> Undefined型：undefined<br />
>> Object对象型，有以下几种：
>>> Array数组： []、 [1,2,3], ['123', true, null, undefined]<br />
>>> Function方法： function fn() {}<br />
>>> Object： {}、{ a: 1 }、{ a: '1' }、{a: 1, b: true, c: null}<br />

> c).初识js
>> ```js
>> <script>
>>     /* 定义变量的几种方式 */
>>     var a       // a = undefined （因为没有赋值）
>>     var a,b
>>     var a = 1, b = [], c = true
>>     // 错误示范
>>     var 12A = 1 // 不能以数字开头
>>     var if = 1 // 不能以关键字开头if、for、while、return...等
>>
>>     /* if条件语句 */
>>     var a = 1
>>     if (a < 2) {  // 1 < 2 吗？ 小于为true， 否则为false
>>         console.log(1) // console.log在控制台打印日志，调试用
>>     } else if (a > 2) {
>>         console.log(3)
>>     } else {
>>         console.log(2) // a === 2
>>     }
>>
>>     /* 逻辑运算符, 与(并且)：&&、或：||、非(取反)： ! */
>>     var a = [], b = undefined, c = 0
>>     a && c  // a 与 c 都要为true的时候，结果为 true。 这里 c 为false，所以结果为false
>>     b && c  //
>>     !(b && c)  // a、c都为true，结果为true。这里b、c都为false。结果为false，但是 ！表示取反，所以结果为true
>>     a || c  // a或者c有一个为true的时候，结果为true。这里，a为true，结果为true，c都可以不用管
>>
>> </script>
>> ```
>> :::tip  练习1
>>     定义6个变量对应6大基本类型
>>     用if条件语句、switch语句、运算符 分别做组合练习
>> :::

<!-- > 3.条件语句
>> ```js
>> var a = 3
>>
>> // if条件语句
>> if (a < 2) {
>>     console.log(1)
>> } else if (a > 2) {
>>     console.log(3)
>> } else {
>>     // a === 2
>>     console.log(2)
>> }
>>
>> // switch条件语句
>> switch(a) {
>>     case 1:
>>         console.log(1)
>>         break;
>>     case 2:
>>         console.log(2)
>>         break;
>>     default:
>>         console.log(3)
>>         break;
>> }
>>
>> // 三元表达式。
>> // a > 2 吗，大于的话走 ? 后面的，否则走 ：后面的。可以在里面继续嵌套 三元表达式
>> a > 2 ? (a > 3 ? console.log(4) : console.log(3)) : console.log(1)
>> ```

> 4.for循环、while循环等<br />
> 5.事件function<br />
>> ```js
>>function fn(params) {
>>    console.log(params)
>>}
>>
>>fn(1) // 1
>>```
> 6.运算符<br />
>> ==、===、!==、>、>=、<、<=、&&、||、!
>>```js
>>var a = 1, b = '1', c = 2, d = 0, e = undefined
>>
>>a == b // true
>>a === b // false，因为a是number类型，b是string类型
>>a !== c // true, 不等于
>>if (a && d) {
>>    console.log(true)
>>} else {
>>    // false , 1可以转换成布尔类型true，d转换后是0。&&并且，a和b都是true，否则就输出false
>>    // 0、null、undefined、都是false
>>    console.log(false)
>>}
>>``` -->

## 二. 面试题
<a href="https://blog.csdn.net/Jet_Lover/article/details/115637795" target="_bnlank">点击跳转，加速了解相关知识</a>


### 1. 练习
> 练习1
>> <image-preview imgUrl="web/html/练习1.png" width="200"></image-preview>

> 练习2
>> <image-preview imgUrl="web/html/练习2.png" width="200"></image-preview>

> 练习3
>> <image-preview imgUrl="web/html/练习3.png" width="200"></image-preview>

> 练习4
>> 给定一个url，自定义一个function，调用该方法能够将url的参数用Object的形式打印出来 <br />
>> 'http://localhost:8000/#/pages/order/list?params1=123&params2=456' <br />
>> 提示：字符串的split <br />
>> <image-preview imgUrl="web/html/练习4.png" width="200"></image-preview>


## 三. 常用js语法补充

### 1. 了解es6语法


#### a). 变量申明
> 图1
>> <image-preview imgUrl="web/html/let.png" width="200"></image-preview>

> 图2
>> <image-preview imgUrl="web/html/let2.png" width="200"></image-preview>

```js

/***************************var、let、const的区别*************************/
// 1. var可以提升变量声明(let、const没有此概念)，相当于：
var a1;
console.log(a1) // 这时候a1还没有赋值，依然是undefined
a1 = 1

// 2. 块级作用域（花括号），例如：
for(var i = 0; i < 3; i++) {
}
console.log(i)
// 不包含函数作用域，注意块级作用域和函数作用域的区别

/***************************var、let、const的区别*************************/


```

#### b). 模板字符串
```js
// 要求 c = 1234566
var a = 123
var b = 456
var c = a + b   // 579
var c = `${a}${b}` // '123456'

// 用法： 1. 反引号；2. 变量要用 ${} 包裹起来
var d = `aaaaaaa${a}bbbbbb${b}` // aaaaaaa123bbbbbb456
```

#### c). 扩展运算符
```js
// 展示形式三个点： ...
var a = [1,2,3]
var b = [...a, 4] // [1,2,3,4]

var c = {a: 1, b: 2}
var d = { ...c, d: 4 } // { a:1, b:2, c:3, d:4 }

// 组合用法，完成数组的去重
var a1 = [1,2,43,2,3,12,5,6]
var a2 = [...new Set(a1)] // [1, 2, 43, 3, 12, 5, 6]
```

#### d). 解构赋值
```js
// 当要取2的时候，a[1]，根据数组的下标选择
var a = [1,2,3,4]

// 还可以
var [a0, a1, ...other] = a
// 表示把 1赋值给a0，2赋值给a1，剩下来的3和4做为一个数组[3,4]赋值给other, 即：
console.log(a0) // 1
console.log(a1) // 2
console.log(other) // [3, 4]

```

#### e). 箭头函数
```js
function fn (a, b) {
    var c = a + b
    console.log(a, b)
}
// 形式转换：
const fn = (a, b) => {
    var c = a + b
    console.log(a, b)
}
// 简化：如果只有一个语句，花括号可省略
const fn = (a, b) => console.log(a, b)
// 简化：如果参数只有一个，括号也能省略
const fn = a => console.log(a)

// 注意：箭头函数自带return，即：
const fn = (a, b) => a + b
var total = fn(1,2)
console.log(total) // 3

// 相当于：
function fn (a, b) {
    return a + b
}
fn(1,2) // 3
```

#### f. promise对象

<div id="html-promise"></div>

```js
    function time() {
        setTimeout(() => {
            console.log(2)
        }, 1000)
    }
    function fn () {
        console.log(1)
        time()
        console.log(3)
    }
    // 调用fn，会得出什么值？
    fn()
    // 答案： 先打印 1、3， 1秒后再打印 2. 如下图
```
> <image-preview imgUrl="web/html/promise1.png" width="200"></image-preview>

```js
// 需求：现在要求按顺序打印出 1、2、3。该如何操作？只需要修改time方法即可
function time() {
    return new Promise((rl, rj) => {
        setTimeout(() => {
            console.log(2)
            rl('可传参数') // 表示代码走到这里，promise成功，并执行 then方法
            // rj() // 表示代码这里，抛出异常错误，并执行catch方法
        }, 1000)
    })
}
// 方式一：
function fn () {
    console.log(1)
    // 链式结构
    time().then(('接收传来的参数') => {
        console.log(3)
    })
}
// 方式二：
async function fn() {
    console.log(1)
    await time()
    console.log(3)
}

// 第二种方法，添加await的方法必须是peomise对象，我们在time方法中return了promise，所以可以用
// 添加了await的 父级(花括号)的开始 加上 async。这两者配套使用的，不能缺少任意一个


// 最终整理如下：
new Promise((rl, rj) => {
    // todo ...
    if ('满足成功的条件') {
        // rl({a: 1})
    }
    if ('满足失败的条件') {
        // rj('失败了')
    }
}).then((data) => {
        console.log(data) // { a: 1 }
    }).catch((err) => {
        console.log(err) // 失败了
    }).finally(() => {
        // 不管成功、失败，最后必然要走到这里，注意：这里没有参数
        console.log(4)
    })



```

#### g. 其他
> <a href="https://m.php.cn/article/471327.html">更详细的es6新特性，点击跳转</a>


## 四. localStorage 和 sessionStorage

> 说明： storage是浏览器提供的缓存方法 <br />
> 区别：前者只有主动清除，才会消失；后者关闭浏览器或刷新网页后，就会消失
```js
    // 存值
    let test1 = '一个值'
    // 这里 test1 必须是 字符串, 如果使用了对象、数组等。必需转为字符串
    // let test1 = JSON.stringify({ a: 1 })
    localStorage.setItem('test', test1)


    // 取值
    let res = localStorage.getItem('test')
    console.log(res) // '一个值'
    // 如果前面存入的时候是 数组、对象，那么还要转换成json格式， 即：
    // JSON.parse(res) // { a: 1 }

    // 删除指定的缓存
    localStorage.removeItem('test')
    // 删除所有
    localStorage.clear()

    // sessionStorage 只需要把 localStorage 替换即可
```
> <image-preview imgUrl="web/html/localstorage1.png" width="200"></image-preview>
> <image-preview imgUrl="web/html/sessionStorage1.png" width="200"></image-preview>


## 五. 补充

### 封装
> 方法体的疯转
> import 和 export 的使用