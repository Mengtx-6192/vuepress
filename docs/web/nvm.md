# nvm


## 一．Node版本管理器nvm

### <a href="https://github.com/coreybutler/nvm-windows/releases" target="_blank">1. 下载地址</a>
>> <image-preview imgUrl="web/nvm/download.png" width="200"></image-preview>

### 2.下载解压后安装，完成后ctrl+R输入cmd
>> <image-preview imgUrl="web/nvm/cmd.png" width="200"></image-preview>

### 3.打开后在命令行中输入nvm -v查看版本
>> <image-preview imgUrl="web/nvm/nvm-v.png" width="200"></image-preview>



### 4.Nvm常用命令
```txt
nvm install 版本号 安装指定版本的node，例：nvm install 12.14.0
nvm install stable 同上，安装的是最新稳定版
nvm ls   查看所有已经安装的版本，例如已经安装了12.14.0，就会显示12.14.0
nvm use 版本号 切换node版本，例：nvm use 12.14.0
nvm uninstall 版本号 卸载指定版本的node，和第一步相反。例：nvm uninstall 12.14.0
nvm --version 查看nvm版本
nvm cache clear 清空nvm缓存目录
```


## 二．也可以直接安装指定node版本
> 作用：由于vue或其他项目所使用的node版本不同，nvm可随意且方便的切换到各个版本
> <a href="http://nodejs.cn" target="_blank">2.1 地址：http://nodejs.cn</a>