const isProduction = ["production", "prod"].includes(process.env.NODE_ENV);

let evnmode = process.env.npm_config_evnmode;

let items = [
    { text: "vpn", link: "/vpn/", type: "pageio" },
    { text: "vue", link: "/vue/", type: "pageio, study" },
    // { text: 'es6', link: '/es6/', type: 'pageio' },
    // { text: 'js', link: '/js/', type: 'pageio' },
    { text: "node", link: "/node/", type: "pageio" },
    { text: "css", link: "/css/", type: "pageio" },
    { text: "web", link: "/web/", type: "pageio, study" },
    { text: "python", link: "/python/", type: "pageio" },
    // { text: 'test', link: '/test1/', type: 'pageio' },
    // { text: '期货', link: '/future/', type: 'pageio' },
    { text: "股票", link: "/gupiao/", type: "gupiao" },
    { text: "npm", link: "/npm/", type: "pageio, study" },
].filter((v) => (evnmode ? v.type.indexOf(evnmode) > -1 : true));

console.log(`环境：${process.env.NODE_ENV}, evnmode: ${evnmode}`, items);

let configList = [
    { text: "主页", link: "/" },
    {
        text: "内容分类",
        items,
    },
    // { text: '关于', link: '/about/' },
    // { text: 'Github', link: 'https://mengtx-6192.github.io' },
];
module.exports = {
    title: "回到开始",
    description: "mtx",
    dest: "public",
    markdown: {
        lineNumbers: true,
    },
    head: [
        ["link", { rel: "icon", href: "/img/logo.ico" }],
        ["link", { rel: "mainifest", href: "/maifest.json" }],
    ],
    themeConfig: {
        log: "", // 图片链接或资源目录
        nav: configList,
        sidebar: {
            "/vpn/": [""],
            "/vue/": ["", "vue"],
            "/es6/": [""],
            "/js/": ["", "skills"],
            "/node/": ["", "exports和export"],
            "/css/": ["", "sass"],
            "/web/": [
                "html",
                "git",
                "nvm",
                "nrm",
                // "note",
                // "webpack",
                "linux",
                "",
                "study",
                "markdown",
            ],
            "/python/": [
                "",
                "for、while",
                "import和格式符",
                "数组、字典",
                "方法体",
                "类与继承",
                "文件读、写",
                "爬虫",
                "selenium",
                "time",
                "多协程、队列",
            ],
            "/test1/": [""],
            "/future/": [""],
            "/gupiao/": [
                "",
                "norm",
                "action",
                "risk",
                // "analysis",
            ],
            "/npm/": ["", "publish"],
        },
        sidebarDepth: 4, // 将 #、## 的标题显示到左侧侧边栏
        lastUpdated: "更新时间", // 显示更新
    },
};
