# 风险识别

## 诱多阳线

> 介绍：
<details>
  <summary><mark><font color=darkred>1. 条件</font></mark></summary>
  <div>顶部大阳线</div>
  <div>缩量</div>
</details>
<details>
  <summary><mark><font color=darkred>2. 举例</font></mark></summary>
  <image-preview imgUrl="gupiao/risk/youduo1.png" ></image-preview>
  <image-preview imgUrl="gupiao/risk/youduo2.png" ></image-preview>
  <image-preview imgUrl="gupiao/risk/youduo3.png" ></image-preview>
</details>

## 顶部滞涨，诱多阳线

> 介绍：
<details>
  <summary><mark><font color=darkred>1. 条件</font></mark></summary>
  <div>长时间上涨后</div>
  <div>高位上涨动力减弱，多个k线实体都较小，且上影线增多(可阴可阳)</div>
  <div>出现下缺口，之后几天中如果有出现阳线但没有回补，是最后的卖点</div>
</details>
<details>
  <summary><mark><font color=darkred>2. 举例</font></mark></summary>
  <image-preview imgUrl="gupiao/risk/youduo3-1.png" ></image-preview>
  <image-preview imgUrl="gupiao/risk/youduo3-2.png" ></image-preview>
  <image-preview imgUrl="gupiao/risk/youduo3-3.png" ></image-preview>
</details>

## 阴包阳

> 介绍：
<details>
  <summary><mark><font color=darkred>1. 条件</font></mark></summary>
  <div>顶部大阴线</div>
  <div>阴线开盘价 > 阳线收盘价；阴线收盘价 < 阳线开盘价</div>
</details>
<details>
  <summary><mark><font color=darkred>2. 举例</font></mark></summary>
  <image-preview imgUrl="gupiao/risk/yby1.png" ></image-preview>
</details>

## 诱空阴线

> 介绍：
<details>
  <summary><mark><font color=darkred>1. 条件</font></mark></summary>
  <div>底部大阴线</div>
  <div>缩量</div>
</details>
<details>
  <summary><mark><font color=darkred>2. 举例</font></mark></summary>
  <!-- <image-preview imgUrl="gupiao/risk/youduo1.png" ></image-preview>
  <image-preview imgUrl="gupiao/risk/youduo2.png" ></image-preview> -->
</details>


## 风险识别5

> 介绍：
<details>
  <summary><mark><font color=darkred>1. 条件</font></mark></summary>
  <image-preview imgUrl="gupiao/risk/risk5.png" width='200'></image-preview>
  <div>长时间上涨后</div>
  <div>高位连续阳线后的第一根阴线</div>
  <div>阴线收盘价低于最后一根阳线开盘价</div>
  <div>且低于倒数第二根阳线的1/2</div>
</details>
<details>
  <summary><mark><font color=darkred>2. 举例</font></mark></summary>
  <image-preview  width='500' imgUrl="gupiao/risk/example5-1.png" ></image-preview>
  <image-preview  width='500' imgUrl="gupiao/risk/example5-2.png" ></image-preview>
</details>


## 风险识别6

> 介绍：
<details>
  <summary><mark><font color=darkred>1. 条件</font></mark></summary>
  <image-preview imgUrl="gupiao/risk/risk6.png" width='200'></image-preview>
  <div>避免错过卖点,或卖早了(还将继续上涨)</div>
  <div>缺口之后的阳线,不一定非要碰到缺口</div>
</details>
<details>
  <summary><mark><font color=darkred>2. 举例</font></mark></summary>
  <image-preview  width='500' imgUrl="gupiao/risk/example6-1.png" ></image-preview>
  <image-preview  width='500' imgUrl="gupiao/risk/example6-2.png" ></image-preview>
</details>


## 风险识别7

> 介绍：
<details>
  <summary><mark><font color=darkred>1. 条件</font></mark></summary>
  <div>顶部中、大阴线</div>
  <div>放量（和近期比较）</div>
</details>
<details>
  <summary><mark><font color=darkred>2. 举例</font></mark></summary>
  <image-preview  width='500' imgUrl="gupiao/risk/example7-1.png" ></image-preview>
  <image-preview  width='500' imgUrl="gupiao/risk/example7-2.png" ></image-preview>
</details>